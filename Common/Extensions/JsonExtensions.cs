﻿using System.Text.Json;

namespace Common.Extensions
{
    public static class JsonExtensions
    {
        public static string ToJsonString(this object obj)
        {
            return obj == null ? (string)null :
            JsonSerializer.Serialize(obj, new JsonSerializerOptions
            {
                IgnoreNullValues = true
            });
        }
    }
}
