﻿namespace Common.Helpers
{
    public enum GenderType
    {
        Male = 0,
        Female = 1
    }
}