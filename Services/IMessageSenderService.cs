﻿using System.Threading.Tasks;

namespace Services
{
    public interface IMessageSenderService
    {
        Task SendEmailAsync(string toEmail, string subject, string message, bool isMessageHtml = false);
    }
}