﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Common.Helpers;
using Microsoft.Extensions.Options;

namespace Services
{
    public class MessageSenderService : IMessageSenderService
    {
        #region Dependency Injection

        private readonly Credentials _credentials;

        #endregion

        #region Constructor
        public MessageSenderService(IOptions<Credentials> credentials)
        {
            _credentials = credentials.Value;
        } 
        #endregion

        public Task SendEmailAsync(string toEmail, string subject, string message, bool isMessageHtml = false)
        {
            using (var client = new SmtpClient())
            {

                var credentials = new NetworkCredential
                {
                    UserName = _credentials.UserName, // without @gmail.com
                    Password = _credentials.Password
                };

                client.Credentials = credentials;
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.EnableSsl = true;

                using var emailMessage = new MailMessage
                {
                    To = { new MailAddress(toEmail) },
                    From = new MailAddress(_credentials.UserName + "@gmail.com"), // with @gmail.com
                    Subject = subject,
                    Body = message,
                    IsBodyHtml = isMessageHtml
                };

                client.Send(emailMessage);
            }

            return Task.CompletedTask;
        }

    }
}
