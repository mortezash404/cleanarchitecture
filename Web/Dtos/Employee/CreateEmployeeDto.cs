﻿using Common.Helpers;

namespace Web.Dtos.Employee
{
    public class CreateEmployeeDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public GenderType GenderType { get; set; }
    }
}
