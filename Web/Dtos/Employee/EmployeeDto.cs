﻿namespace Web.Dtos.Employee
{
    public class EmployeeDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
