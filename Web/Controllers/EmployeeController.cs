﻿using System.Threading.Tasks;
using AutoMapper;
using Entities;
using EntityFrameworkCore.Managers.Employee;
using EntityFrameworkCore.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Web.Dtos.Employee;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        #region Dependency Injection
        private readonly IMapper _mapper;
        private readonly IEmployeeManager _employeeManager;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        #endregion

        #region Constructor
        public EmployeeController(IMapper mapper, IEmployeeManager employeeManager, IUnitOfWorkManager unitOfWorkManager)
        {
            _mapper = mapper;
            _employeeManager = employeeManager;
            _unitOfWorkManager = unitOfWorkManager;
        } 
        #endregion

        // GET: api/Employees
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Employee>>> GetEmployees()
        //{
        //    return await _context.Employees.ToListAsync();
        //}

        // GET: api/Employees/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EmployeeDto>> GetEmployee(int id)
        {
            var employee = await _employeeManager.GetByIdAsync(id);

            if (employee == null)
            {
                return NotFound();
            }

            return _mapper.Map<EmployeeDto>(employee);
        }

        // PUT: api/Employees/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutEmployee(int id, Employee employee)
        //{
        //    if (id != employee.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(employee).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!EmployeeExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Employees
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<OkObjectResult> PostEmployee(CreateEmployeeDto input)
        {
            var employee = _mapper.Map<Employee>(input);

            await _employeeManager.InsertAsync(employee);

            await _unitOfWorkManager.CompleteAsync();

            return Ok(employee);
        }

        //// DELETE: api/Employees/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Employee>> DeleteEmployee(int id)
        //{
        //    var employee = await _context.Employees.FindAsync(id);
        //    if (employee == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Employees.Remove(employee);
        //    await _context.SaveChangesAsync();

        //    return employee;
        //}

        //private bool EmployeeExists(int id)
        //{
        //    return _context.Employees.Any(e => e.Id == id);
        //}
    }
}
