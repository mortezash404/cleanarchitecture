﻿using AutoMapper;
using Entities;
using Web.Dtos.Employee;

namespace Web.MapProfile
{
    public class EmployeeMapProfile : Profile
    {
        public EmployeeMapProfile()
        {
            CreateMap<CreateEmployeeDto, Employee>();
            CreateMap<Employee, EmployeeDto>();
        }
    }
}
