﻿using System.Threading.Tasks;

namespace EntityFrameworkCore.Managers.Employee
{
    public interface IEmployeeManager
    {
        Task<Entities.Employee> GetByIdAsync(int id);
        Task InsertAsync(Entities.Employee employee);
    }
}
