﻿using System.Threading.Tasks;

namespace EntityFrameworkCore.Managers.Employee
{
    public class EmployeeManager : IEmployeeManager
    {
        #region Dependency Injection
        private readonly AppDbContext _context;
        #endregion

        #region Constructor
        public EmployeeManager(AppDbContext context)
        {
            _context = context;
        } 
        #endregion

        public async Task<Entities.Employee> GetByIdAsync(int id)
        {
            return await _context.Employees.FindAsync(id);
        }

        public async Task InsertAsync(Entities.Employee employee)
        {
            await _context.Employees.AddAsync(employee);
        }
    }
}
