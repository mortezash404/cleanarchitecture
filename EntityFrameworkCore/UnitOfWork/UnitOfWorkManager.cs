﻿using System;
using System.Threading.Tasks;

namespace EntityFrameworkCore.UnitOfWork
{
    public class UnitOfWorkManager : IUnitOfWorkManager
    {
        #region Dependency Injection

        private readonly AppDbContext _context;

        #endregion

        #region Constructor
        public UnitOfWorkManager(AppDbContext context)
        {
            _context = context;
        } 
        #endregion

        public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
