﻿using System.Threading.Tasks;

namespace EntityFrameworkCore.UnitOfWork
{
    public interface IUnitOfWorkManager
    {
        Task CompleteAsync();
    }
}
